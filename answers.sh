#!/bin/bash -i
red='\033[0;31m'
nc='\033[0m' # No Color

##############################
# Easy - Google is your friend
##############################

echo -e $'\n'"${red}Print the contents of groceries.txt${nc}"
cat groceries.txt
sleep 2

echo -e $'\n'"${red}Print the first line of groceries.txt${nc}"
head -n 1 groceries.txt
sleep 2

echo -e $'\n'"${red}Print out how many processors threads you have.${nc}"
nproc
sleep 2

echo -e $'\n'"${red}Print the numbers 1 through 10.${nc}"
seq 10
sleep 2

echo -e $'\n'"${red}Print a calender showing the current, previous, and next monthes.${nc}"
cal -3
sleep 2

echo -e $'\n'"${red}Print the sizes and names of files in this directory in order of decreasing size.${nc}"
du * | sort -grk 1
sleep 2

echo -e $'\n'"${red}Print only Amos's grocery list in groceries.txt.${nc}"
awk '{print $1}' groceries.txt
sleep 2

echo -e $'\n'"${red}Print Amos's grocery list, but replace bananas with apples.${nc}"
awk '{print $1}' groceries.txt | sed -e 's/bananas/apples/g'
sleep 2

echo -e $'\n'"${red}Print the everything but the first line of groceries.txt${nc}"
tail -n +2 groceries.txt
sleep 2

echo -e $'\n'"${red} Print the name of all files in this directory (recursively) with filenames containing the letter 't' ${nc}"
find . -name "*t*"
sleep 2

echo -e $'\n'"${red}Print a sorted list of the 10 commands used most often.${nc}"
history | awk '{a[$2]++}END{for(i in a){print a[i] " " i}}' | sort -rn | head
sleep 2

#################################
# Medium - Requires some thinking
#################################

echo -e $'\n'"${red}Print the speed in MHz that each CPU is operatring at, in order from fastest to slowest.${nc}"
cat /proc/cpuinfo | grep 'cpu MHz' | awk '{print $4}' | sort -g -r
sleep 2

echo -e $'\n'"${red}Create a static fileserver for the files in this directory and view it in a browser.${nc}"
python -m SimpleHTTPServer & firefox http://localhost:8000/ &
sleep 10

echo -e $'\n'"${red}List the files in this directory, 3 per line.${nc}"
ls | xargs -n 3 echo
sleep 2

echo -e $'\n'"${red}Print the difference between Amos's and Bernadette's grocery lists in groceries.txt.${nc}"
diff -y <(tail -n 4 groceries.txt | awk '{print $1}' | sort ) <(tail -n 4 groceries.txt | awk '{print $2}' | sort)
sleep 2

echo -e $'\n'"${red}Print the common item between Amos's and Bernadette's grocery lists.${nc}"
awk '{print $1} {print $2}' groceries.txt | sort | uniq -d
sleep 2

echo -e $'\n'"${red}Combine Amos's and Bernadette's grocery lists with no duplications.${nc}"
tail -n +2 groceries.txt | awk '{print $1} {print $2}' | sort | uniq
sleep 2

echo -e $'\n'"${red}Print a HTML representation of the directory tree (from this directory).${nc}"
tree -H -
sleep 2

echo -e $'\n'"${red}Display graph a showing the hierarchy between Animalia, Chordata, Mammalia, and Reptilia.${nc}"
echo "digraph G {Animalia->Chordata->Mammalia; Chordata->Reptilia}" | dot -Tpng | display png:-
sleep 2

echo -e $'\n'"${red}Print the Gettysburg Address from the ocr folder ${nc}"
tesseract ocr/gettysburg.tiff out &>/dev/null && cat out.txt
sleep 2

#######################
# Challenge - Good luck
#######################

echo -e $'\n'"${red}Play 'Mary Had a Little Lamb'.${nc}"
for i in 800 700 600 700 800 800 800 700 700 700 800 800 800 800 700 600 700 800 800 800 700 700 800 700 600; do play -n synth 0.1 sine $i &> /dev/null; done
sleep 2

echo -e $'\n'"${red}Print groceries.txt with the first and last letters of each food item swapped.${nc}"
head -n1 groceries.txt && tail -n+2 groceries.txt | sed 's/\(\W\|^\)\(\w\)\(\w* \?\w*\)\(\w\)\(\W\|$\)/\1\4\3\2\5/g'
sleep 2

echo -e $'\n'"${red}Create a graphical sine wave animation and run/display it.${nc}"
gnuplot -e "set terminal gif; do for [t=0:100] {set output sprintf('f-%d.gif', t); plot sin(x+t*2*pi/100); }" && convert -delay 2 -loop 0 $(ls f-*.gif | sort -k 1.3 -n) gif:- | gifview -a

echo -e $'\n'"${red}Print all the unique words in from the images in the ocr folder.${nc}"
find . -name "*.tiff" | xargs -I '{}' tesseract '{}' '{}' &> /dev/null &&  find ocr -name "*.txt" | xargs sed -e 's/\s\+/\n/g' >> temp && sort temp | uniq | xargs -n 10 echo && rm temp
sleep 2

echo -e $'\n'"${red}Untar many.tar.gz then rename all duplicate files (by content, not recursively) to have the name of the original file plus a suffix indicating the file name of the duplicate.${nc}"
tar xf many.tar.gz && find . -maxdepth 1 -type f | xargs sha1sum | sort | uniq --all-repeated=separate -w40 | sed 's/[^ ]* *\.\///' | awk 'NR == 1 || count == 0 {orig=$0;count++;next;} /^$/ {count="0";next;} {printf "mv %s %s-dup%d-%s\n",$0,orig,count,$0;count++;}' | xargs -I{} bash -c {}
sleep 2

##############
# Just for fun
##############
echo -e $'\n'"${red}Watch the command line version of Star Wars.${nc}"
sleep 2
telnet towel.blinkenlights.nl

