#!/bin/bash -i
red='\033[0;31m'
nc='\033[0m' # No Color

##############################
# Easy - Google is your friend
##############################

echo -e $'\n'"${red}Print the contents of groceries.txt${nc}"
echo "Skip"
sleep 2

echo -e $'\n'"${red}Print the first line of groceries.txt${nc}"
echo "Skip"
sleep 2

echo -e $'\n'"${red}Print out how many processors threads you have.${nc}"
echo "Skip"
sleep 2

echo -e $'\n'"${red}Print the numbers 1 through 10.${nc}"
echo "Skip"
sleep 2

echo -e $'\n'"${red}Print a calender showing the current, previous, and next monthes.${nc}"
echo "Skip"
sleep 2

echo -e $'\n'"${red}Print the sizes and names of files in this directory in order of decreasing size.${nc}"
echo "Skip"
sleep 2

echo -e $'\n'"${red}Print only Amos's grocery list in groceries.txt.${nc}"
echo "Skip"
sleep 2

echo -e $'\n'"${red}Print Amos's grocery list, but replace bananas with apples.${nc}"
echo "Skip"
sleep 2

echo -e $'\n'"${red}Print the everything but the first line of groceries.txt${nc}"
echo "Skip"
sleep 2

echo -e $'\n'"${red} Print the name of all files in this directory (recursively) with filenames containing the letter 't' ${nc}"
echo "Skip"
sleep 2

echo -e $'\n'"${red}Print a sorted list of the 10 commands used most often.${nc}"
echo "Skip"
sleep 2

#################################
# Medium - Requires some thinking
#################################

echo -e $'\n'"${red}Print the speed in MHz that each CPU is operatring at, in order from fastest to slowest.${nc}"
echo "Skip"
sleep 2

echo -e $'\n'"${red}Create a static fileserver for the files in this directory and view it in a browser.${nc}"
echo "Skip"
sleep 10

echo -e $'\n'"${red}List the files in this directory, 3 per line.${nc}"
echo "Skip"
sleep 2

echo -e $'\n'"${red}Print the difference between Amos's and Bernadette's grocery lists in groceries.txt.${nc}"
echo "Skip"
sleep 2

echo -e $'\n'"${red}Print the common item between Amos's and Bernadette's grocery lists.${nc}"
echo "Skip"
sleep 2

echo -e $'\n'"${red}Combine Amos's and Bernadette's grocery lists with no duplications.${nc}"
echo "Skip"
sleep 2

echo -e $'\n'"${red}Print a HTML representation of the directory tree (from this directory).${nc}"
echo "Skip"
sleep 2

echo -e $'\n'"${red}Display graph a showing the hierarchy between Animalia, Chordata, Mammalia, and Reptilia.${nc}"
echo "Skip"
sleep 2

echo -e $'\n'"${red}Print the Gettysburg Address from the ocr folder ${nc}"
echo "Skip"
sleep 2

#######################
# Challenge - Good luck
#######################

echo -e $'\n'"${red}Play 'Mary Had a Little Lamb'.${nc}"
echo "Skip"
sleep 2

echo -e $'\n'"${red}Print groceries.txt with the first and last letters of each food item swapped.${nc}"
echo "Skip"
sleep 2

echo -e $'\n'"${red}Create a graphical sine wave animation and run/display it.${nc}"
echo "Skip"
sleep 2

echo -e $'\n'"${red}Print all the unique words in from the images in the ocr folder.${nc}"
echo "Skip"
sleep 2

echo -e $'\n'"${red}Untar many.tar.gz then rename all duplicate files (by content, not recursively) to have the name of the original file plus a suffix indicating the file name of the duplicate.${nc}"
echo "Skip"
sleep 2

##############
# Just for fun
##############
echo -e $'\n'"${red}Watch the command line version of Star Wars.${nc}"
echo "Skip"

